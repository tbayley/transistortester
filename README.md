# Transistor Tester #
The transistor tester is based on the open hardware “transistortester” project:
https://www.mikrocontroller.net/articles/AVR_Transistortester

The most up-to-date source code and documentation can be obtained by cloning the
Subversion repository: svn://mikrocontroller.net/transistortester. In November
2019 the repository is at revision 811.

I have built two versions of transistor tester hardware. One has a 128x64 pixel
OLED display that uses the SSD1306 display driver with I2C interface. The other
has a 128x64 pixel STN LCD display that uses a ST7920 display drive with a 4-bit
parallel interface.

Both hardware variants use a 5v / 16MHz Arduino Pro Mini board that is fitted
with an Atmel AtMega328P processor. The following modifications are required:

1.  Replace the 100nF capacitor between AREF and GND by a 1nF capacitor. AREF is
    pin 20 of the ATmega328P microcontroller.
2.  Remove the LED and resistor that are attached to pin PB5(SCK). If left the
    LED seriously affects measurement accuracy. PB5(SCK) is pin 17 of the
    Atmega328P microcontroller.
3.  Optionally remove the power LED and resistor that are attached to the 5V
    regulator output, to reduce power consumption.

For further details see the design notes:
[ttester-notes.docx](doc/ttester-notes.docx) and the transistor tester manual
[ttester.pdf](doc/ttester.pdf).

Hardware schematics in the _"schematic"_ subdirectory were drafted using
[DesignSpark PCB](https://www.rs-online.com/designspark/pcb-software). Veroboard
layouts in the _"layout"_ subdirectory were created using the software
[DIY Layout Creator](https://github.com/bancika/diy-layout-creator).